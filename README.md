## Example to send errors and exceptions via sentry sdk

Related to [issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1737)

### Instructions
```bash
virtualenv env
source env/bin/activate
pip -r requirements.txt
DSN=http://your_dsn python send_exception.py
```