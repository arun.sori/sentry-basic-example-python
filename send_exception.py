#!/usr/bin/env python
import sentry_sdk, os, sys
from sentry_sdk import HttpTransport

import certifi

class InsecureHTTPTransport(HttpTransport):

    # INSECURE: Overriding this to only disabling for self hosted cases
    def _get_pool_options(self, ca_certs):
        # type: (Optional[Any]) -> Dict[str, Any]
        insecure_ok = os.getenv("INSECURE_OK", False).lower() == "true"
        cert_reqs = "CERT_REQUIRED"
        if insecure_ok is True:
            cert_reqs = "CERT_NONE"
        return {
            "num_pools": 2,
            "cert_reqs": cert_reqs,
            "ca_certs": ca_certs or certifi.where(),
        }

def init_sentry(dsn: str) -> None:

    sentry_sdk.init(
        dsn=dsn,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0,
        transport=InsecureHTTPTransport,
	    debug=True,
    )


def failing_function() -> None:
    raise Exception("An exception")

if __name__ == "__main__":
    dsn = os.getenv("DSN")
    if dsn is None:
        print("empty dsn; exiting")
        sys.exit(1)

    init_sentry(dsn)
    try:
        failing_function()
    except Exception as e:
        sentry_sdk.capture_exception(e)

